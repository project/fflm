<?php

/**
 * @file
 * FFLM preloader field handler. 
 */

class fflm_handler_field_preloader extends views_handler_field {
  function init(&$view, $options) {
    parent::init($view, $options);
    $schema = drupal_get_schema('files');
    foreach ($schema['fields'] as $field => $data) {
      if ($field != 'fid') {
        $this->additional_fields[$field] = $field;
      }
    }
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['label']['#access'] = FALSE;
    $form['exclude']= array(
      '#type' => 'value',
      '#value' => TRUE,
    );
    $form['alter']['#access'] = FALSE;
    $form['empty']['#access'] = FALSE;
    $form['empty_zero']['#access'] = FALSE;
    $form['hide_empty']['#access'] = FALSE;
  }

  /**
   * Load & cache multiple file data.
   */
  function pre_render($values) {
    $threads = array();
    $schema = drupal_get_schema('files');
    foreach ($schema['fields'] as $field => $data) {
      $fields[] = $field;
    }
    
    foreach ($values as $value) {
      $file = array();
      foreach ($fields as $field) {
        if (!empty($value->{$this->aliases[$field]})) {
          $file[$field] = $value->{$this->aliases[$field]}; 
        }
      }
      // Filefield caches non-existing files too, but it looks like a useless
      // operation because they all have fid = 0 in the cache thus the cache 
      // is unusable in this case.
      if (!empty($file)) {
        $file['fid'] = $value->{$this->field_alias};
        $file = (object) $file;
        foreach (module_implements('file_load') as $module) {
          if ($module != 'field') {
            $function = $module .'_file_load';
            $function($file);
          }
        }
        // Update FileField module cache.
        _field_file_cache($file);
      }
    }
  }
}
